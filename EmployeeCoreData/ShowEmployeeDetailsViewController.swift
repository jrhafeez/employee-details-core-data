//
//  ShowEmployeeDetailsViewController.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 14/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit
import CoreData

class ShowEmployeeDetailsViewController: UIViewController {
    
    var userName: String?
    var userLastName: String?
    var email: String?
    var age: String?
    var sex: String?
    var sexSelection = ["Male", "Female", "Other"]
    var sexString = ""
    let pickerViewForSex = UIPickerView()
    
    @IBOutlet weak var showFirstNameTextField: UITextField!
    @IBOutlet weak var showLastNameTextField: UITextField!
    @IBOutlet weak var showSexTextField: UITextField!
    @IBOutlet weak var showAgeTextField: UITextField!
    @IBOutlet weak var showEmailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Employee's Record"
        
        showFirstNameTextField.text = userName
        showLastNameTextField.text = userLastName
        showEmailTextField.text = email
        showSexTextField.text = sex
        showAgeTextField.text = age
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self, action: #selector(updateRecordButton))
        
        sexPickerView()
    }

    @objc func updateRecordButton() {
        let appDelegateUpdateObject = UIApplication.shared.delegate as! AppDelegate
        let contextUpdateObject = appDelegateUpdateObject.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfile")
        fetchRequest.predicate = NSPredicate(format: "name = %@",userName!)
        do {
            let results = try contextUpdateObject.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 {
                results![0].setValue(showFirstNameTextField.text, forKey: "name")
                results![0].setValue(showLastNameTextField.text, forKey: "lastName")
                results![0].setValue(showEmailTextField.text, forKey: "email")
                results![0].setValue(showAgeTextField.text, forKey: "age")
                results![0].setValue(showSexTextField.text, forKey: "sex")
            }
        } catch {
            print("Error while updating.")
        }
        do {
            try contextUpdateObject.save()
            print("User Profile Updated.")
        } catch {
            print("Error while saving data.")
        }
        self.navigationController?.popViewController(animated: true)
    }
}

// picker view
extension ShowEmployeeDetailsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sexSelection.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sexSelection[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sexString = sexSelection[row]
        showSexTextField.text = sexString
    }
    
    func sexPickerView() {
        pickerViewForSex.delegate = self
        showSexTextField.inputView = pickerViewForSex
    }
}
