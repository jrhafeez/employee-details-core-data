//
//  EmployeeDetailViewController.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 14/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit
import CoreData

class EmployeeDetailViewController: UIViewController {
    
    var userList: [NSManagedObject] = []
    var userName = ""

    @IBOutlet weak var employeeDetailTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        employeeDetailTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.title = "Employees"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchingEmployeeList()
        self.employeeDetailTableView.reloadData()
    }
    
    @IBAction func addNewEmployeeButton(_ sender: Any) {
        let employeeAddNavigation = self.storyboard?.instantiateViewController(withIdentifier: "AddEmployeeViewController") as? AddEmployeeViewController
        self.navigationController?.pushViewController(employeeAddNavigation!, animated: true)
    }
    
    // fetching employee data
    func fetchingEmployeeList() {
        let appDelegateObject = UIApplication.shared.delegate as! AppDelegate
        let contextObject = appDelegateObject.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "UserProfile")
        request.returnsObjectsAsFaults = false
        print(request.returnsObjectsAsFaults)
        
        do {
           userList = try contextObject.fetch(request) as! [NSManagedObject]
        } catch {
            print("Error while fetching record..")
        }
    }
}

// showing data in table cell
extension EmployeeDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = userList[indexPath.row]
        let employeeDetailCell = employeeDetailTableView.dequeueReusableCell(withIdentifier:  "employeeDetailCell") as? EmployeeDetailTableViewCell
        employeeDetailCell?.employeeNameLabel.text = user.value(forKey: "name") as? String
        employeeDetailCell?.profileButton.tag = indexPath.row
        employeeDetailCell?.profileButton.addTarget(self, action: #selector(clickProfileButton), for: .touchUpInside)
        employeeDetailCell?.noteButton.tag = indexPath.row
         employeeDetailCell?.noteButton.addTarget(self, action: #selector(clickNoteButton), for: .touchUpInside)
        return employeeDetailCell!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let appDelegateDeleteObject = UIApplication.shared.delegate as! AppDelegate
            let contextDeleteObject = appDelegateDeleteObject.persistentContainer.viewContext
            contextDeleteObject.delete(userList[indexPath.row])
            do {
                try contextDeleteObject.save()
            } catch {
                print("Error while deleting record...")
            }
        }
        userList.remove(at: indexPath.row)
        employeeDetailTableView.deleteRows(at: [indexPath], with: .fade)
    }
    
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let editEmployeeRecord = UIContextualAction(style: .normal, title: "Edit") { (action, view, nil) in
//            let employeeEditNavigation = self.storyboard?.instantiateViewController(withIdentifier: "EditEmployeeViewController") as? EditEmployeeViewController
//            //let employee = self.employeeList[indexPath.row]
//            self.navigationController?.pushViewController(employeeEditNavigation!, animated: true)
//            print("Edit action worked.")
//        }
//        editEmployeeRecord.backgroundColor = UIColor.blue
//        return UISwipeActionsConfiguration(actions: [editEmployeeRecord])
//    }

    // profile button action defination on cell
    @objc func clickProfileButton(sender: UIButton) {
        let showDetailNavigation = self.storyboard?.instantiateViewController(withIdentifier: "ShowEmployeeDetailsViewController") as? ShowEmployeeDetailsViewController
        showDetailNavigation?.userName = (userList[sender.tag].value(forKey: "name") as? String)!
        showDetailNavigation?.userLastName = (userList[sender.tag].value(forKey: "lastName") as? String)!
        showDetailNavigation?.email = (userList[sender.tag].value(forKey: "email") as? String)!
        showDetailNavigation?.age = (userList[sender.tag].value(forKey: "age") as? String)!
        showDetailNavigation?.sex = (userList[sender.tag].value(forKey: "sex") as? String)!
        self.navigationController?.pushViewController(showDetailNavigation!, animated: true)
    }
    
    // note button action defination on cell
    @objc func clickNoteButton(sender: UIButton) {
        let noteListNavigation = self.storyboard?.instantiateViewController(withIdentifier: "NotesListViewController") as? NotesListViewController
        print("hello")
        noteListNavigation?.userName = (userList[sender.tag].value(forKey: "name") as? String)!
        print("hi")
        self.navigationController?.pushViewController(noteListNavigation!, animated: true)
    }
}
