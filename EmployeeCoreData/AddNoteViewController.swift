//
//  AddNoteViewController.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 18/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit
import CoreData

class AddNoteViewController: UIViewController {
    var addNoteName = ""
    
    @IBOutlet weak var noteTextViewField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Add Note"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveNoteButton))
    }
    
    @objc func saveNoteButton() {
        let appDelegateAddNoteObject = UIApplication.shared.delegate as! AppDelegate
        let contextAddNoteObject = appDelegateAddNoteObject.persistentContainer.viewContext
        let newNote = NSEntityDescription.insertNewObject(forEntityName: "Note", into: contextAddNoteObject)
        
        newNote.setValue(noteTextViewField.text, forKey: "note")
        newNote.setValue(addNoteName, forKey: "name")
        do {
            try contextAddNoteObject.save()
            print("Data Saved.")
        } catch {
            print("Error while saving record..")
        }
        self.navigationController?.popViewController(animated: true)
    }
}
