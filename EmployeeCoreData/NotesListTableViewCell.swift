//
//  NotesListTableViewCell.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 18/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit

class NotesListTableViewCell: UITableViewCell {

    @IBOutlet weak var noteCellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
