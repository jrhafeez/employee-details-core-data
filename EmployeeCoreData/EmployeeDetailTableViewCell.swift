//
//  EmployeeDetailTableViewCell.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 14/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit



class EmployeeDetailTableViewCell: UITableViewCell {    
    
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var noteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
