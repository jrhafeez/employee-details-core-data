//
//  AddEmployeeViewController.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 14/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit
import CoreData


class AddEmployeeViewController: UIViewController {
    
    var sexSelection = ["Male", "Female", "Other"]
    var ageSelection = ["infant", "toddler", "kid", "teenager", "young adult", "simply called adult", "senior citizens"]
    var sexString = ""
    var ageString = ""
    let pickerViewForSex = UIPickerView()
    let pickerViewForAge = UIPickerView()

    @IBOutlet weak var employeeNameTextField: UITextField!
    @IBOutlet weak var employeeSexTextField: UITextField!
    @IBOutlet weak var employeeLastNameTextField: UITextField!
    @IBOutlet weak var employeeAgeTextField: UITextField!
    @IBOutlet weak var employeeEmailTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add New Employee"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addEmployeeRecordButton))
        sexPickerView()
    }
    
    // adding data for employee
        @objc func addEmployeeRecordButton() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "UserProfile", into: context)
        
        newUser.setValue(employeeNameTextField.text, forKey: "name")
        newUser.setValue(employeeLastNameTextField.text, forKey: "lastName")
        newUser.setValue(employeeSexTextField.text, forKey: "sex")
        newUser.setValue(employeeAgeTextField.text, forKey: "age")
        newUser.setValue(employeeEmailTextField.text, forKey: "email")
            

        
        do {
            try context.save()
            print("Data Saved.")
        } catch {
            print("Error while saving record..")
        }
        self.navigationController?.popViewController(animated: true)
    }
}

// picker view
extension AddEmployeeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sexSelection.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sexSelection[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sexString = sexSelection[row]
        employeeSexTextField.text = sexString
    }
    
    func sexPickerView() {
        pickerViewForSex.delegate = self
        employeeSexTextField.inputView = pickerViewForSex
    }
}
