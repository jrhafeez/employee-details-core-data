//
//  NotesListViewController.swift
//  EmployeeCoreData
//
//  Created by cl-macmini-45 on 18/02/19.
//  Copyright © 2019 cl-macmini-45. All rights reserved.
//

import UIKit
import CoreData

class NotesListViewController: UIViewController {
    
    var noteList: [NSManagedObject] = []
//    var user: UserProfile?
    var userName = ""

    @IBOutlet weak var noteTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noteTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.title = "Notes"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addNewNoteButton))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchingNoteList()
        self.noteTableView.reloadData()
    }
    
   @objc func addNewNoteButton() {
        let addNoteNavigation = self.storyboard?.instantiateViewController(withIdentifier: "AddNoteViewController") as? AddNoteViewController
        addNoteNavigation?.addNoteName = userName
        self.navigationController?.pushViewController(addNoteNavigation!, animated: true)
    }
    
     //fetching note data
    func fetchingNoteList() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "Note")
        let predicate = NSPredicate(format: "name == %@", userName)
        request.predicate = predicate
        do {
            noteList = try context.fetch(request) as! [NSManagedObject]
        } catch {
            print("Could not fetch.")
        }
        print("predicate working fine")
    }
}

// showing data in table cell
extension NotesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noteList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let noteCell = noteTableView.dequeueReusableCell(withIdentifier:  "noteCell") as? NotesListTableViewCell
        noteCell?.noteCellLabel.text = noteList[indexPath.row].value(forKey: "note") as? String
        print("showing in table cell")
        return noteCell!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let appDelegateNoteDeleteObject = UIApplication.shared.delegate as! AppDelegate
            let contextNoteDeleteObject = appDelegateNoteDeleteObject.persistentContainer.viewContext
            contextNoteDeleteObject.delete(noteList[indexPath.row])
            do {
                try contextNoteDeleteObject.save()
            } catch {
                print("Error while deleting record...")
            }
        }
        noteList.remove(at: indexPath.row)
        noteTableView.deleteRows(at: [indexPath], with: .fade)
    }
}
